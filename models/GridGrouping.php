<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grid_grouping".
 *
 * @property int $id
 * @property int $supplier_id
 * @property int $category_id
 * @property string $product_name
 * @property int $unit_price
 * @property int $unit_in_stock
 *
 * @property Supplier $supplier
 * @property Category $category
 */
class GridGrouping extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grid_grouping';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplier_id', 'category_id', 'product_name', 'unit_price', 'unit_in_stock'], 'required'],
            [['supplier_id', 'category_id', 'unit_price', 'unit_in_stock'], 'integer'],
            [['product_name'], 'string', 'max' => 200],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplier_id' => 'Supplier ID',
            'category_id' => 'Category ID',
            'product_name' => 'Product Name',
            'unit_price' => 'Unit Price',
            'unit_in_stock' => 'Unit In Stock',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
