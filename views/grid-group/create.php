<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GridGrouping */

$this->title = 'Create Grid Grouping';
$this->params['breadcrumbs'][] = ['label' => 'Grid Groupings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid-grouping-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
