<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GridGrouping */

$this->title = 'Update Grid Grouping: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Grid Groupings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="grid-grouping-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
