<?php
use kartik\widgets\ActiveForm;
use kartik\label\LabelInPlace;
use kartik\dialog\Dialog;
use kartik\icons\Icon;
use kartik\cmenu\ContextMenu;
use kartik\dropdown\DropdownX;
use kartik\nav\NavX;
use kartik\widgets\Select2;
use kartik\money\MaskMoney;
use kartik\widgets\DepDrop;
use kartik\social\FacebookPlugin;
use kartik\social\GooglePlugin;
use kartik\social\GoogleAnalytics;

use yii\helpers\Html;
use Yii\helpers\Url;

$this->title = 'Krajee Yii Extensions';

?>
<style type="text/css">
    .bottom{
        margin-bottom: 20px;
    }
</style>
<div class="site-contact">
    <h1 style="text-align: center;">Krajee Yii Extensions</h1><hr>
    <h3>Label In Place Extension</h3><hr>
    <?php $form = ActiveForm::begin()?>
 
 <div class="row bottom">
     <div class="col-md-6 col-sm-6">
        <?php
            // Basic usage without active form or model.
            echo LabelInPlace::widget(['name'=>'username', 'label'=>'Username']);
        ?>
     </div>
     <div class="col-md-6 col-sm-6">
         <?php  
            // Hide indicators and make a plain label
            echo LabelInPlace::widget([
                'name' => 'customer', 
                'label'=>'Customer',
                'defaultIndicators'=>false
            ]);
         ?>
     </div>
 </div>

 <div class="row bottom">
     <div class="col-md-6 col-sm-6">
         <?php 
            // Style your own labels.
            echo LabelInPlace::widget([
                'name'=>'phone', 
                'label'=>'<i class="glyphicon glyphicon-phone"></i> Phone number',
                'encodeLabel'=> false
            ]);

         ?>
     </div>
     <div class="col-md-6 col-sm-6 bottom">
         <?php  
            // Change label positions, label indicators, and other plugin options.
            echo LabelInPlace::widget([
                'name'=>'Enter code', 
                'type'=>LabelInPlace::TYPE_TEXT,
                'label'=>'<i class="glyphicon glyphicon-lock"></i> Type your code',
                'encodeLabel'=>false,
                'pluginOptions'=>[
                    'labelPosition'=>'down',
                    'labelArrowDown'=>' <i class="glyphicon glyphicon-chevron-down"></i>',
                    'labelArrowUp'=>' <i class="glyphicon glyphicon-chevron-up"></i>',
                    'labelArrowRight'=>' <i class="glyphicon glyphicon-chevron-right"></i>',
                ]
            ]);

         ?>
     </div>
 </div>

 <div class="row bottom">
     <div class="col-md-6 col-sm-6">
         <?php 
            // Advanced usage with ActiveForm and model. Use three input types (html5, text, and textarea). 
            // Automatically convert model attribute labels. 
            // Note: You must deactivate the label rendering in the default active field template.
             
            $config = ['template'=>"{input}\n{error}\n{hint}"]; // config to deactivate label for ActiveField
             
            echo $form->field($model, 'email', $config)->widget(LabelInPlace::classname(),[
                'type' => LabelInPlace::TYPE_HTML5,
                'options' => ['type' => 'email', 'class'=>'form-control']
            ]);

         ?>
     </div>
     <div class="col-md-6 col-sm-6">
         <?php echo $form->field($model, 'phone', $config)->widget(LabelInPlace::classname());   ?>
     </div>
 </div>

 <div class="row bottom">
     <div class="col-md-12 col-sm-12">
         <?php echo $form->field($model, 'notes', $config)->widget(LabelInPlace::classname(), [
            'type' => LabelInPlace::TYPE_TEXTAREA
        ]); ?>
     </div>
 </div>
 <?php ActiveForm::end(); ?>
</div>
<hr>
<h3>Dependent Drop Downs Extension</h3><hr>
<?php
// Normal parent select
echo $form->field($model, 'cat')->dropDownList([1=>'Option 1',2=>'Option 2'], ['id'=>'cat-id','name'=>'depdrop_parents']);
 
// Dependent Dropdown
echo $form->field($model, 'subcat')->widget(DepDrop::classname(), [
     'options' => ['id'=>'subcat-id'],
     'pluginOptions'=>[
         'depends'=>['cat-id'],
         'placeholder' => 'Select...',
         'url' => Url::to(['/grid-group/subcat'])
     ]
 ]);
?>
<hr>
<h3>Icons Extension</h3><hr>

<?= Icon::show('music', [], Icon::BSG) ?> Music
<?= Icon::show('user', [], Icon::BSG) ?> User
<?= Icon::show('home', [], Icon::BSG) ?> Home
<?= Icon::show('inbox', [], Icon::BSG) ?> Inbox
<?= Icon::show('send', [], Icon::BSG) ?> Send
<?= Icon::show('adjust', [], Icon::BSG) ?> Adjust
<?= Icon::show('tint', [], Icon::BSG) ?> Tint
<?= Icon::show('plane', [], Icon::BSG) ?> Plane
<?= Icon::show('shopping-cart', [], Icon::BSG) ?> Shopping Cart
<?= Icon::show('bullhorn', [], Icon::BSG) ?> Bullhorn
<hr>
<h3>Dialogs Extension</h3><hr>
<?php 

// widget with default options
echo Dialog::widget();
 
// buttons for testing the krajee dialog boxes
$btns = <<< HTML
<button type="button" id="btn-alert" class="btn btn-info">Alert</button>
<button type="button" id="btn-confirm" class="btn btn-warning">Confirm</button>
<button type="button" id="btn-prompt-1" class="btn btn-primary">Prompt-1</button>
<button type="button" id="btn-prompt-2" class="btn btn-primary">Prompt-2</button>
<button type="button" id="btn-dialog" class="btn btn-default">Dialog</button>
<hr>
<label>Test Yii Confirm (Click Link): </label>
<!-- Yii Links Confirmation Dialog Override Example Markup --> 
<a href="http://demos.krajee.com" id="btn-yii-link" 
   data-confirm="Are you sure you want to navigate to Krajee Demos Home Page?">
   Krajee Demos Home
</a>
HTML;
echo $btns;
 
// javascript for triggering the dialogs
$js = <<< JS
$("#btn-alert").on("click", function() {
    
    krajeeDialog.alert("This is a Krajee Dialog Alert!")
});
$("#btn-confirm").on("click", function() {
    krajeeDialog.confirm("Are you sure you want to proceed?", function (result) {
        if (result) {
            alert('Great! You accepted!');
        } else {
            alert('Oops! You declined!');
        }
    });
});
$("#btn-prompt-1").on("click", function() {
    krajeeDialog.prompt({label:'Provide reason', value: 'This is an initial reason.', placeholder:'Upto 30 characters...', maxlength: 30}, function (result) {
        if (result) {
            if (result === 'This is an initial reason.') {
                alert('Ok! Accepting the initial reason');
            } else {
                alert('Great! You provided a reason');
            }
        } else {
            alert('Oops! You declined!');
        }
    });
});
$("#btn-prompt-2").on("click", function() {
    krajeeDialog.prompt({type: 'password', label:'Authenticate', placeholder:'Enter password to authenticate...'}, function (result) {
        if (result) {
            alert('Great! You provided a password:'+ result);
        } else {
            alert('Oops! You declined to provide a password!');
        }
    });
});
$("#btn-dialog").on("click", function() {
    krajeeDialog.dialog(
        'This is a <b>custom dialog</b>. The dialog box is <em>draggable</em> by default and <em>closable</em> ' +
        '(try it). Note that the Ok and Cancel buttons will do nothing here until you write the relevant JS code ' +
        'for the buttons within "options". Exit the dialog by clicking the cross icon on the top right.',
        function (result) {alert(result);}
    );
});
JS;
 
// register your javascript
$this->registerJs($js);

?>
<hr>
<h3>MPDF Extension</h3><hr>
<?= Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> Privacy Statement', ['/grid-group/mpdf-demo-1'], [
    'class'=>'btn btn-danger', 
    'target'=>'_blank', 
    'data-toggle'=>'tooltip', 
    'title'=>'Will open the generated PDF file in a new window'
]); ?>
<hr>

<h3>Drop Down X Extension</h3><hr>
<?php
echo Html::beginTag('div', ['class'=>'dropdown']);
echo Html::button('Dropdown Left <span class="caret"></span></button>', 
    ['type'=>'button', 'class'=>'btn btn-default', 'data-toggle'=>'dropdown']);
echo DropdownX::widget([
    'items' => [
        ['label' => 'Action', 'url' => '#'],
        ['label' => 'Submenu 1', 'items' => [
            ['label' => 'Action', 'url' => '#'],
            ['label' => 'Another action', 'url' => '#'],
            ['label' => 'Something else here', 'url' => '#'],
            '<li class="divider"></li>',
            ['label' => 'Submenu 2', 'items' => [
                ['label' => 'Action', 'url' => '#'],
                ['label' => 'Another action', 'url' => '#'],
                ['label' => 'Something else here', 'url' => '#'],
                '<li class="divider"></li>',
                ['label' => 'Separated link', 'url' => '#'],
            ]],
        ]],
        ['label' => 'Something else here', 'url' => '#'],
        '<li class="divider"></li>',
        ['label' => 'Separated link', 'url' => '#'],
    ],
]); 
echo Html::endTag('div');
 
 
echo Html::beginTag('div', ['class'=>'text-right dropdown']); // align right
echo Html::button('Dropdown Right <span class="caret"></span></button>', 
    ['type'=>'button', 'class'=>'btn btn-default', 'data-toggle'=>'dropdown']);
echo DropdownX::widget([
    'options'=>['class'=>'pull-right'], // for a right aligned dropdown menu
    'items' => [
        ['label' => 'Action', 'url' => '#'],
        ['label' => 'Submenu 1', 'items' => [
            ['label' => 'Action', 'url' => '#'],
            ['label' => 'Another action', 'url' => '#'],
            ['label' => 'Something else here', 'url' => '#'],
            '<li class="divider"></li>',
            ['label' => 'Submenu 2', 'items' => [
                ['label' => 'Action', 'url' => '#'],
                ['label' => 'Another action', 'url' => '#'],
                ['label' => 'Something else here', 'url' => '#'],
                '<li class="divider"></li>',
                ['label' => 'Separated link', 'url' => '#'],
            ]],
        ]],
        ['label' => 'Something else here', 'url' => '#'],
        '<li class="divider"></li>',
        ['label' => 'Separated link', 'url' => '#'],
    ],
]); 
echo Html::endTag('div');

 ?>
<hr>
<h3>Contex Menu Extension</h3><hr>
<?php 
$items = [
    ['label'=>'Action', 'url'=>'#'],
    ['label'=>'Another action', 'url'=>'#'],
    ['label'=>'Something else here', 'url'=>'#'],
    '<li class="divider"></li>',
    ['label'=>'Separated link', 'url'=>'#'],
];
 
// Basic context menu usage on a specific text within a paragaph.
ContextMenu::begin(['items'=>$items]);
echo '<span class="kv-context">Right click here.</span>';
ContextMenu::end();
 
// Context menu usage on a div container
ContextMenu::begin(['items'=>$items, 'options'=>['tag'=>'div']]);
echo '<div class="well">Right click anywhere inside this container.</div>';
ContextMenu::end();
 
// Activate context menu within a div but not for `spans` within the div
$script = <<< 'JS'
function (e, element, target) {
    e.preventDefault();
    if (e.target.tagName == 'SPAN') {
        e.preventDefault();
        this.closemenu();
        return false;
    }
    return true;
}
JS;
ContextMenu::begin([
    'items'=>$items,
    'options'=>['tag'=>'div'],
    'pluginOptions'=>['before'=>$script]
]);
echo '<div class="well">Right click anywhere inside this container but note that
    <span class="text-danger">context is disabled in this highlighted span element</span></div>';
ContextMenu::end();
?>
<hr>
<h3>Navigation Extension</h3><hr>
<?php
echo NavX::widget([
    'options' => ['class' => 'nav nav-pills'],
    'items' => [
        ['label' => 'Action', 'url' => '#'],
        ['label' => 'Submenu', 'items' => [
            ['label' => 'Action', 'url' => '#'],
            ['label' => 'Another action', 'url' => '#'],
            ['label' => 'Something else here', 'url' => '#'],
        ]],
        ['label' => 'Something else here', 'url' => '#'],
        '<li class="divider"></li>',
        ['label' => 'Separated link', 'url' => '#'],
    ],
    'encodeLabels' => false
]);

 ?>
<hr>
<h3>Money Masked Input Fields Extension</h3><hr>
<?php
// Money mask widget with ActiveForm and model validation rule (amounts between 1 to 100000). 
// Initial value is set to 1400.50. Note the prefix and suffix settings.
echo $form->field($model, 'amount')->widget(MaskMoney::classname(), [
    'pluginOptions' => [
        'prefix' => '$ ',
        'suffix' => ' ¢',
        'allowNegative' => false
    ]
]);
 
// Money mask widget without ActiveForm or model and settings defaulted from 
// `maskMoneyOptions` in `Yii::$app->params`.  Initial value is set to 20322.22. 
echo MaskMoney::widget([
    'name' => 'amount_drcr',
    'value' => 20322.22
]);
 
// Example of money mask widget for a different locale using comma `,` as the decimal separator and 
// dot `.` as the thousands separator. Initial value is set to `0.01`.
echo MaskMoney::widget([
    'name' => 'amount_german',
    'value' => 0.01,
    'pluginOptions' => [
        'prefix' => '€ ',
        'thousands' => '.',
        'decimal' => ',',
        'precision' => 2
    ],
]);
 
// Money mask widget with zero precision along with prefix and suffix.
echo MaskMoney::widget([
    'name' => 'amount_rounded_1',
    'value' => 1000,
    'pluginOptions' => [
        'prefix' => '$ ',
        'suffix' => ' €',
        'precision' => 0
    ]
]);
 
// A disabled MaskMoney input.
echo MaskMoney::widget([
    'name' => 'amount_1',
    'value' => 28239.35,
    'disabled' => true
]);
 
// Money mask widget with a default placeholder and `allowEmpty` set to `true`.
echo MaskMoney::widget([
    'name' => 'amount_ph_1',
    'value' => null,
    'options' => [
        'placeholder' => 'Enter a valid amount...'
    ],
    'pluginOptions' => [
        'allowZero' => false,
        'allowEmpty' => true
    ]
]);
?>
<hr>
<h3>Social Media Extension</h3><hr>
<p>Includes. <b>Disqus, Facebook, Google+, Twitter etc</b> Requires app ids</p>
<?= GoogleAnalytics::widget(['id'=>'TRACKING_ID', 'domain'=>'TRACKING_DOMAIN']) ?>
<hr>