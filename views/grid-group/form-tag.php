 <?php
 // add this in your view
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\Supplier;
use app\models\Category;
// Vertical Form
$form = ActiveForm::begin([
    'id' => 'form-signup',
    'type' => ActiveForm::TYPE_VERTICAL
]);
 

// Implement a feedback icon
echo $form->field($model, 'email_2', [
    'feedbackIcon' => [
        'default' => 'envelope',
        'success' => 'ok',
        'error' => 'exclamation-sign',
        'defaultOptions' => ['class'=>'text-primary']
    ]
])->textInput(['placeholder'=>'Enter a valid email address...']);

// Prepend an addon text
echo $form->field($model, 'email', ['addon' => ['prepend' => ['content'=>'@']]]);

// Append an addon text
echo $form->field($model, 'amount_paid', [
    'addon' => ['append' => ['content'=>'.00']]
]);

// Formatted addons (like icons)
echo $form->field($model, 'phone', [
    'addon' => [
        'prepend' => [
            'content' => '<i class="glyphicon glyphicon-phone"></i>'
        ]
    ]
]);

// Formatted addons (inputs)
echo $form->field($model, 'phone_2', [
    'addon' => [
        'prepend' => [
            'content' => '<input type="radio">'
        ]
    ]
]);

// Formatted addons (buttons)
echo $form->field($model, 'phone_3', [
    'addon' => [
        'prepend' => [
            'content' => Html::button('Go', ['class'=>'btn btn-primary']),
            'asButton' => true
        ]
    ]
]);


 ?>
 <div class="row">
     <div class="col-md-6">
        <?= $form->field($model, 'desc')->textarea(); ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'password')->passwordInput() ?>
    </div>
 </div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'email')->input('email') ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'uploadFile')->fileInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?=   $form->field($model, 'population[]')->checkboxList(
            ['a' => 'Item A', 'b' => 'Item B', 'c' => 'Item C']
   ); ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'population')->radioList(array('1'=>'One',2=>'Two')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?php
        $listData =[1=>'John',2=>'Doe']; 
        echo $form->field($model, 'name')->dropDownList(
            $listData, 
            ['prompt'=>'Choose...']
            ); ?>
    </div>
    <div class="col-md-6"></div>
</div>
<div class="row">
    <div class="col-md-6"></div>
    <div class="col-md-6"></div>
</div>
<div class="row">
    <div class="col-md-6"></div>
    <div class="col-md-6"></div>
</div>
<div class="row">
    <div class="col-md-6"></div>
    <div class="col-md-6"></div>
</div>
<div class="row">
    <div class="col-md-6"></div>
    <div class="col-md-6"></div>
</div>
 
 <?php ActiveForm::end(); ?>