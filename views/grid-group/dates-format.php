 <?php
 // add this in your view
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\Supplier;
use app\models\Category;
use kartik\date\DatePicker;

// Vertical Form
$form = ActiveForm::begin([
    'id' => 'form-signup',
    'type' => ActiveForm::TYPE_VERTICAL
]);
 
 


?>

<div class="row">
    <div class="col-md-6">
    <?php
// usage without model
echo '<label>Check Issue Date</label>';
echo DatePicker::widget([
    'name' => 'check_issue_date', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select issue date ...'],
    'pluginOptions' => [
        'format' => 'dd-M-yyyy',
        'todayHighlight' => true
    ]
]);

 ?>
</div>
<div class="col-md-6">
    <?php
echo '<label class="control-label">Birth Date</label>';
echo DatePicker::widget([
    'name' => 'dp_3',
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'value' => '23-Feb-1982',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd-M-yyyy'
    ]
]);
    ?>
</div>
</div>
<div class="row">
    <div class="col-md-6">
    <!-- date range -->
    <?php
echo '<label class="control-label">Valid Dates</label>';
echo DatePicker::widget([
    'name' => 'from_date',
    'value' => '01-Feb-1996',
    'type' => DatePicker::TYPE_RANGE,
    'name2' => 'to_date',
    'value2' => '27-Feb-1996',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd-M-yyyy'
    ]
]);
    ?>
</div>
<div class="col-md-6">
    <?php
// With Prepend
$layout1 = <<< HTML
    <span class="input-group-addon">Birth Date</span>
    {picker}
    <span class="input-group-addon">bef</span>
    {remove}
    <span class="input-group-addon">aft</span>
    {input}
HTML;
echo DatePicker::widget([
    'name' => 'dp_addon_1',
    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
    'value' => '23-Feb-1982',
    'layout' => $layout1,
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'dd-M-yyyy'
    ]
]); 

?>
</div>
</div>

<div class="row">
    <div class="col-md-6">
      
    </div>
    <div class="col-md-6">
        <label>
            Date format with options
        </label>
        <?php
// With Append
$layout2 = <<< HTML
    <span class="input-group-addon">Birth Date</span>
    {input}
    <span class="input-group-addon">bef</span>
    {picker}
    <span class="input-group-addon">aft</span>
    {remove}
HTML;
echo DatePicker::widget([
    'name' => 'dp_addon_2',
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'value' => '23-Feb-1982',
    'layout' => $layout2,
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'dd-M-yyyy'
    ]
]);
 
        ?>
    </div>
</div>

<div class="col-md-12">
      <label>
            Date Range
        </label>
        <?php
// With Range

// With Range
$layout3 = <<< HTML
    <span class="input-group-addon">From Date</span>
    {input1}
    <span class="input-group-addon">aft</span>
    {separator}
    <span class="input-group-addon">To Date</span>
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;
 
echo DatePicker::widget([
    'type' => DatePicker::TYPE_RANGE,
    'name' => 'dp_addon_3a',
    'value' => '01-Jul-2015',
    'name2' => 'dp_addon_3b',
    'value2' => '18-Jul-2015',
    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
    'layout' => $layout3,
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'dd-M-yyyy'
    ]
]);  ?>
</div>
<div class="row">
    <?php
echo '<label class="control-label">Select date range</label>';
echo DatePicker::widget([
    'model' => $model,
    'attribute' => 'from_date',
    'attribute2' => 'to_date',
    'options' => ['placeholder' => 'Start date'],
    'options2' => ['placeholder' => 'End date'],
    'type' => DatePicker::TYPE_RANGE,
    'form' => $form,
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
    ]
]); 
 
// Setting datepicker for your regional language (e.g. fr for French)
echo '<label class="control-label">Date de Naissance</label>';
echo DatePicker::widget([
    'name' => 'date_10',
    'language' => 'fr',
    'options' => ['placeholder' => 'Enter birth date ...'],
    'pluginOptions' => [
        'autoclose' => true,
    ]
]);
 
// Highlight today, show today button, change date format
echo '<label class="control-label">Birth Date</label>';
echo DatePicker::widget([
    'name' => 'date_11',
    'options' => ['placeholder' => 'Enter birth date ...'],
    'pluginOptions' => [
        'todayHighlight' => true,
        'todayBtn' => true,
        'format' => 'dd-M-yyyy',
        'autoclose' => true,
    ]
]);
 
// Show week numbers and disable certain days of week (e.g. weekends)
echo '<label class="control-label">Birth Date</label>';
echo DatePicker::widget([
    'name' => 'date_12',
    'value' => '31-Dec-2010',
    'pluginOptions' => [
        'calendarWeeks' => true,
        'daysOfWeekDisabled' => [0, 6],
        'format' => 'dd-M-yyyy',
        'autoclose' => true,
    ]
]);
 
// Change orientation of datepicker as well as markup type
echo '<label class="control-label">Setup Date</label>';
echo DatePicker::widget([
    'name' => 'date_12',
    'value' => '08/10/2004',
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'pluginOptions' => [
        'orientation' => 'top right',
        'format' => 'mm/dd/yyyy',
        'autoclose' => true,
    ]
]);
 
 
// Multiple Dates Selection
echo '<label class="control-label">Select Dates</label>';
echo DatePicker::widget([
    'name' => 'date_12',
    'value' => '08/10/2004',
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'pluginOptions' => [
        'format' => 'mm/dd/yyyy',
        'multidate' => true,
        'multidateSeparator' => ' ; ',
    ]
]);
    ?>
</div>


 <?php ActiveForm::end(); ?>