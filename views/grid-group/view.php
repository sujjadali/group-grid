<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GridGrouping */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Grid Groupings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid-grouping-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'supplier_id',
            'category_id',
            'product_name',
            'unit_price',
            'unit_in_stock',
        ],
    ]) ?>

</div>
