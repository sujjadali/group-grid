<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Supplier;
use app\models\Category;
/* @var $this yii\web\View */
/* @var $model app\models\GridGrouping */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grid-grouping-form">

    <?php $form = ActiveForm::begin(); ?>
 
    <?= $form->field($model, 'supplier_id')->dropDownList(
                 ArrayHelper::map(Supplier::find()->orderBy('name')->asArray()->all(), 'id', 'name'), 
                ['prompt'=>'Select...']);
                ?>

<?= $form->field($model, 'category_id')->dropDownList(
                 ArrayHelper::map(Category::find()->orderBy('name')->asArray()->all(), 'id', 'name'), 
                ['prompt'=>'Select...']);
                ?> 

    <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unit_price')->textInput() ?>

    <?= $form->field($model, 'unit_in_stock')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
