use kartik\widgets\ActiveForm;
use kartik\label\LabelInPlace;
 
$form = ActiveForm::begin();
 
// Basic usage without active form or model.
echo LabelInPlace::widget(['name'=>'username', 'label'=>'Username']);
 
// Hide indicators and make a plain label
echo LabelInPlace::widget([
    'name' => 'customer', 
    'label'=>'Customer',
    'defaultIndicators'=>false
]);
 
// Style your own labels.
echo LabelInPlace::widget([
    'name'=>'phone', 
    'label'=>'<i class="glyphicon glyphicon-phone"></i> Phone number',
    'encodeLabel'=> false
]);
 
// Change label positions, label indicators, and other plugin options.
echo LabelInPlace::widget([
    'name'=>'Enter code', 
    'type'=>LabelInPlace::TYPE_TEXT,
    'label'=>'<i class="glyphicon glyphicon-lock"></i> Type your code',
    'encodeLabel'=>false,
    'pluginOptions'=>[
        'labelPosition'=>'down',
        'labelArrowDown'=>' <i class="glyphicon glyphicon-chevron-down"></i>',
        'labelArrowUp'=>' <i class="glyphicon glyphicon-chevron-up"></i>',
        'labelArrowRight'=>' <i class="glyphicon glyphicon-chevron-right"></i>',
    ]
]);
 
// Advanced usage with ActiveForm and model. Use three input types (html5, text, and textarea). 
// Automatically convert model attribute labels. 
// Note: You must deactivate the label rendering in the default active field template.
 
$config = ['template'=>"{input}\n{error}\n{hint}"]; // config to deactivate label for ActiveField
 
echo $form->field($model, 'email', $config)->widget(LabelInPlace::classname(),[
    'type' => LabelInPlace::TYPE_HTML5,
    'options' => ['type' => 'email', 'class'=>'form-control']
]);
echo $form->field($model, 'phone', $config)->widget(LabelInPlace::classname()); 
echo $form->field($model, 'notes', $config)->widget(LabelInPlace::classname(), [
    'type' => LabelInPlace::TYPE_TEXTAREA
]);
 
ActiveForm::end();